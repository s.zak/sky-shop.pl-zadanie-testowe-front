<h1>Instrukcja uruchamiania</h1>
<h2>Potrzebne</h2>
<p>
Upenij się, że masz zainstalowany jakiś <code>serwer http</code>, np WAMP, XAMP etc. Jeżeli nie masz możesz zainstalować http-server poprzez komendę <code>npm install http-server -g</code>. Instrukcja zadania znajduje się wewnątrz aplikacji po jej otworzeniu.
</p>
<h2>Pobierz repozytorium</h2>
<pre><code>$ git clone https://gitlab.com/s.zak/sky-shop.pl-zadanie-testowe-front.git
$ cd sky-shop-zadanie-testowe-front
$ http-server
</code>
</pre>
<p>
W razie pytań proszę o kontakt na maila szymon.zak@sky-shop.pl
</p>