ngApp.directive('mdlDrawerClose',function(){
  return {
    restrict: 'AC',
    link: function($scope,$element){
      $element.bind('click',function(){
        [].forEach.call(document.querySelectorAll('.mdl-layout__drawer,.mdl-layout__obfuscator'),function(mdl){
          mdl.classList.toggle('is-visible');
        });
      });
    }
  };
});
