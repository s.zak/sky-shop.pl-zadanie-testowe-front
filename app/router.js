ngApp.config(['$routeProvider','$locationProvider',function($routeProvider, $locationProvider){
  $routeProvider
    .when('/', {
      templateUrl: 'view/index.html',
      controller: 'IndexCtrl',
      controllerAs: 'index',
      resolveAs: 'Główna'
    })
    .when('/instruction', {
      templateUrl: 'view/instruction.html',
      controller: 'InstructionCtrl',
      controllerAs: 'instruction',
      resolveAs: 'Instrukcja'
    });

    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });
}]);
